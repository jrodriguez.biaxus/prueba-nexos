package com.nexos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.nexos.core.audit.interceptor.AuditWebRequestInterceptor;

/**
 * Core configuration for spring boot application.
 */
@SpringBootApplication
public class Application {

	/**
	 * Starter method
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * Metod to configure web mvc settings, CORS, and the web interceptor
	 *
	 * @return the web mvc configurer bean
	 */
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {

			/** A aplication context injection for the interceptor. */
			@Autowired
			ApplicationContext applicationContext;

			/** The executor service for the interceptor. */
			@Autowired
			TaskExecutor executor;

			/**
			 * CORS configuration for allowing queries from clients like Angular.
			 */
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("*").allowedMethods("*");
			}

			/**
			 * Adds a interceptor. in this case the audit request interceptor.
			 */
			@Override
			public void addInterceptors(InterceptorRegistry registry) {

				registry.addWebRequestInterceptor(new AuditWebRequestInterceptor(applicationContext, executor));
			}
		};
	}

	/**
	 * Thread pool task executor.
	 *
	 * @return the task executor
	 */
	@Bean
	public TaskExecutor threadPoolTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(4);
		executor.setMaxPoolSize(4);
		executor.setThreadNamePrefix("default_task_executor_thread");
		executor.initialize();
		return executor;
	}
}
