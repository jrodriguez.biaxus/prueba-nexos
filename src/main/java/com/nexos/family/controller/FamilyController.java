package com.nexos.family.controller;

import java.util.List;

import javax.servlet.ServletException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.nexos.family.model.Family;
import com.nexos.family.model.Person;
import com.nexos.family.service.FamilyService;

/**
 * Rest controller to expose the services, this contain the services to save
 * persons and query families
 */
@org.springframework.web.bind.annotation.RestController()
public class FamilyController {

	/** The family service, containing all the business logic. */
	@Autowired
	private FamilyService service;

	/**
	 * Find all family in database.
	 *
	 * @return the family list
	 */
	@GetMapping("/families")
	public List<Family> findAllFamily() {
		return service.findAllFamily();
	}

	/**
	 * Adds a person, calls the service to validate and save in database.
	 *
	 * @param person the person to be saved
	 * @return the saved person
	 * @throws ServletException validation errors
	 */
	@PostMapping("/person")
	public Person addPerson(@Valid Person person) throws ServletException {
		return service.savePerson(person);
	}
}
