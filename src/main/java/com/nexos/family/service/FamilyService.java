package com.nexos.family.service;

import java.util.List;

import javax.servlet.ServletException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import com.nexos.family.model.Family;
import com.nexos.family.model.Person;
import com.nexos.family.repository.CoreEntityRepository;
import com.nexos.family.repository.FamilyRepository;

/**
 * A service for contains all the logic operations on entities.
 * 
 * @author jr 20200304
 */
@Service 
public class FamilyService {

	/**
	 * Field to contains the database operations for core entities
	 * 
	 * @author jr 20200304
	 */
	@Autowired
	private CoreEntityRepository repository;

	/**
	 * Field to contains the database operations for family entities, in this case
	 * is for query all entities
	 * 
	 * @author jr 20200304
	 */
	@Autowired
	private FamilyRepository familyRepository;

	/**
	 * Save a person. If the person don't bring a family the family is created and
	 * associated to this person. If the person have a family is saved with the
	 * persisted.
	 *
	 * @param person the person to persist
	 * @return the persisted person
	 * @throws ServletException validation errors
	 */
	public Person savePerson(Person person) throws ServletException {
		if (person.getFamily() == null || person.getFamily().getId() == null) {
			Family f = new Family();
			f.setName("Familia de: " + person.getName());
			repository.saveAndFlush(f);
			person.setFamily(f);
			repository.saveAndFlush(person);
		} else {
			repository.saveAndFlush(person);
		}
		return person;
	}

	/**
	 * Method to load all families to know the available families to add to the
	 * person .
	 *
	 * @return the available family list
	 */
	public List<Family> findAllFamily() {
		return familyRepository.findAll();
	}
}
