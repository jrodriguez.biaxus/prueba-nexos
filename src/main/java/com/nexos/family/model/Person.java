
package com.nexos.family.model;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.nexos.core.model.CoreEntity;

/**
 * A person on a family group
 */
@javax.persistence.Entity
public class Person extends CoreEntity {

	/** The document number. */
	@Column(nullable = false, length = 12)
	@NotEmpty(message = "documentNumber can't be empty")
	@Length(max = 12, message = "documentNumber only can be 12 character length")
	private String documentNumber;

	/** The mobile phone. */
	@Column(nullable = false, length = 10)
	@NotEmpty(message = "mobilePhone can't be empty")
	@Length(max = 10, message = "mobilePhone only can be 10 character length")
	private String mobilePhone;

	/** The phone. */
	@Column(nullable = false, length = 7)
	@NotEmpty(message = "phone can't be empty")
	@Length(max = 7, message = "phone only can be 7 character length")
	private String phone;

	/** The family. */
	@JsonBackReference(value = "personFamily")
	@ManyToOne(fetch = FetchType.LAZY)
	private Family family;

	/**
	 * Instantiates a new person.
	 */
	public Person() {
		//
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumer the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the mobile phone.
	 *
	 * @return the mobile phone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * Sets the mobile phone.
	 *
	 * @param mobilePhone the new mobile phone
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone the new phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the family.
	 *
	 * @return the family
	 */
	public Family getFamily() {
		return family;
	}

	/**
	 * Sets the family.
	 *
	 * @param family the new family
	 */
	public void setFamily(Family family) {
		this.family = family;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Person [documentNumer=" + documentNumber + ", mobilePhone=" + mobilePhone + ", phone=" + phone + ", id="
				+ id + ", name=" + name + "]";
	}

}
