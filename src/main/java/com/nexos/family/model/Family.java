
package com.nexos.family.model;

import java.util.List;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.nexos.core.model.CoreEntity;

/**
 * Family entity. Represents the family group of a person
 * 
 * @author jr 20200304
 */
@javax.persistence.Entity
public class Family extends CoreEntity {

	/** The person list associated to this family group. */
	@JsonManagedReference(value = "personFamily")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "family")
	private List<Person> personList;

	/**
	 * Instantiates a new family.
	 */
	public Family() {
		//
	}

	/**
	 * Gets the person list of this family.
	 *
	 * @return the person list on family
	 */
	public List<Person> getPersonList() {
		return personList;
	}

	/**
	 * Sets the person list of this family.
	 *
	 * @param personList the new person list
	 */
	public void setPersonList(List<Person> personList) {
		this.personList = personList;
	}

	/**
	 * Transform the entity to a string representation
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Family [id=" + id + ", name=" + name + "]";
	}
}
