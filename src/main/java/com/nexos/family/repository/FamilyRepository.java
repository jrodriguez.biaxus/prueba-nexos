package com.nexos.family.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.Repository;

import com.nexos.family.model.Family;

/**
 * Interface containing all the definitions for database operations to family
 * entities
 * 
 * This interface is implemented by the spring data framework on
 * {@link SimpleJpaRepository}
 * 
 * @author jr 20200304
 * 
 * @see SimpleJpaRepository
 * @see JpaRepository
 * @see Repository
 */
public interface FamilyRepository extends Repository<Family, Long> {

	List<Family> findAll();
}
