package com.nexos.family.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.Repository;

import com.nexos.core.model.CoreEntity;

/**
 * Interface containing all the definitions for database operations to core entity  
 * 
 * This interface is implemented by the spring data framework on
 * {@link SimpleJpaRepository}
 * 
 * @author jr 20200304
 * 
 * @see SimpleJpaRepository
 * @see JpaRepository
 * @see Repository
 */
public interface CoreEntityRepository extends Repository<CoreEntity, Long> {

	
	<S extends CoreEntity> S saveAndFlush(S entity);
}
