package com.nexos;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.nexos.family.repository.CoreEntityRepository;

/**
 * This class handle all the exceptions of platform
 */
@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

	/** The application context, to inject repositories when needed. */
	@Autowired
	private ApplicationContext applicationContext;

	/** The executor to do asynchronous tasl. */
	@Autowired
	private TaskExecutor executor;

	/**
	 * Inhertit method of parent ResponseEntityExceptionHandler check the
	 * documentation on parent.
	 * 
	 * The implementation takes the exeption and transforms to the entity Error, and
	 * persist in database with the executor on a async task
	 */
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		final com.nexos.core.error.model.Error e = new com.nexos.core.error.model.Error();
		e.setSessionId(request.getSessionId());
		e.setIp(((ServletWebRequest) request).getRequest().getRemoteAddr());
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		e.setStackTrace(sw.toString());
		
		//async task definition
		executor.execute(() -> {
			e.setName(ex.getClass().getCanonicalName());
			e.setMessage(ex.getMessage());
			e.setTimestamp(Calendar.getInstance().getTime());
			//dependency injection to bring the repository
			CoreEntityRepository repository = applicationContext.getBean(CoreEntityRepository.class);
			repository.saveAndFlush(e);
		});
		return super.handleExceptionInternal(ex, body, headers, status, request);
	}

	/**
	 * Inhertit method of parent ResponseEntityExceptionHandler check the
	 * documentation on parent
	 */
	@Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", Calendar.getInstance().getTime());
		body.put("status", status.value());
		// Get all errors
		List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())
				.collect(Collectors.toList());
		body.put("errors", errors);
		return new ResponseEntity<>(body, headers, status);
	}

	/**
	 * Method to catch the any Exeption, this method
	 *
	 * @param ex      the ex
	 * @param request the request
	 * @return the response entity
	 * @throws Exception the exception
	 */
	@org.springframework.web.bind.annotation.ExceptionHandler(value = { Exception.class })
	public final ResponseEntity<Object> handleGenericException(Exception ex, WebRequest request) throws Exception {
		return handleExceptionInternal(ex, null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
}
