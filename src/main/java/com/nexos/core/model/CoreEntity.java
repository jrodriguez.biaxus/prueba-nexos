
package com.nexos.core.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

/**
 * A core entity to generalize generic fields of entities
 */
@MappedSuperclass
public abstract class CoreEntity {

	/** Identifier for entities, generated value via hibernate sequence. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Integer id;

	/** The name field of entities, is not nullable and max length of 500. */
	@Column(nullable = false, length = 500)
	@NotEmpty(message = "name can't be empty")
	@Length(max = 400, message = "name only can be 400 character length")
	protected String name;

	/**
	 * Gets the entity identifier.
	 *
	 * @return the identifier
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the entity identifier.
	 *
	 * @param id the new identifier
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the entity name.
	 *
	 * @return the entity name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the entity name.
	 *
	 * @param name the new name for the entity
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Transform the entity to a string representation
	 *
	 * @return the string representation
	 */
	@Override
	public String toString() {
		return String.format("CoreEntity[id=%d, name='%s']", id, name);
	}

}
