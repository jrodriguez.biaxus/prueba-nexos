package com.nexos.core.audit.interceptor;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexos.core.audit.filter.HttpServletResponseCopier;
import com.nexos.core.audit.model.Audit;
import com.nexos.family.repository.CoreEntityRepository;

/**
 * This class is a interceptor, this is called on every request to the web
 * application, in this case the request will be processed for auditing
 */
public class AuditWebRequestInterceptor implements WebRequestInterceptor {

	/**
	 * This is a object mapper of jackson to convert some objects in json, and
	 * persist in that way on database.
	 */
	private static final ObjectMapper OM = new ObjectMapper();

	/** The spring application context, to load any bean on the context. */
	private ApplicationContext applicationContext;

	/** The executor executor service to do async task. */
	private TaskExecutor executor;

	/**
	 * Instantiates a new audit web request interceptor, .
	 *
	 * @param applicationContext the application context
	 * @param executor           the executor service
	 */
	public AuditWebRequestInterceptor(ApplicationContext applicationContext, TaskExecutor executor) {
		this.applicationContext = applicationContext;
		this.executor = executor;
	}

	@Override
	public void preHandle(WebRequest request) throws Exception {
		// no implementation needed
	}

	@Override
	public void postHandle(WebRequest request, ModelMap model) throws Exception {
		// no implementation needed
	}

	/**
	 * This method executes on request finish. Is the next step to request process
	 * finish
	 *
	 * @param request the web request
	 * @param ex      a exception if this was threw
	 * @throws Exception a exception if this method throws a exception
	 */
	@Override
	public void afterCompletion(WebRequest request, Exception ex) throws Exception {
		// TODO Auto-generated method stub
		ServletWebRequest servletWebRequest = (ServletWebRequest) request;
		HttpServletRequest httpServletRequest = servletWebRequest.getRequest();

		// attributes
		Map<String, Object> attributes = new HashMap<>();
		Enumeration<String> enumeration = httpServletRequest.getAttributeNames();
		while (enumeration.hasMoreElements()) {
			String item = enumeration.nextElement();
			attributes.put(item, httpServletRequest.getAttribute(item).toString());
		}
		final Audit audit = new Audit();
		audit.setAttributes(OM.writeValueAsString(attributes));
		// method
		audit.setMethod(httpServletRequest.getMethod());

		// url
		audit.setUrl(httpServletRequest.getRequestURL().toString());

		// params
		audit.setParameters(OM.writeValueAsString(httpServletRequest.getParameterMap()));

		// ip
		audit.setIp(httpServletRequest.getLocalAddr());

		// timestamp
		audit.setTimestamp(Calendar.getInstance().getTime());

		// request headers
		Map<String, Object> headers = new HashMap<>();
		Enumeration<String> enumerationHeaders = httpServletRequest.getHeaderNames();
		while (enumerationHeaders.hasMoreElements()) {
			String item = enumerationHeaders.nextElement();
			headers.put(item, httpServletRequest.getHeader(item));
		}
		audit.setRequestHeaders(OM.writeValueAsString(headers));

		// response header
		HttpServletResponse httpServletResponse = servletWebRequest.getResponse();
		headers = new HashMap<>();
		Iterator<String> eHeaders = httpServletResponse.getHeaderNames().iterator();
		while (eHeaders.hasNext()) {
			String item = eHeaders.next();
			headers.put(item, httpServletRequest.getHeader(item));
		}
		audit.setResponseHeaders(OM.writeValueAsString(headers));

		// status
		audit.setResponseStatus(String.valueOf(httpServletResponse.getStatus()));

		// response body
		audit.setResponseBody(getResponseContent(httpServletResponse));

		// name
		audit.setName(httpServletResponse.toString());
		
		//async task definition
		executor.execute(() -> {
			//dependency injection to bring the repository
			CoreEntityRepository repository = applicationContext.getBean(CoreEntityRepository.class);
			repository.saveAndFlush(audit);
		});
	}

	/**
	 * Gets the response content.
	 *
	 * @param response the response
	 * @return the response content
	 */
	private String getResponseContent(HttpServletResponse response) {
		// here the response search for the HttpServletResponseCopier instance, that is
		// the wrapper that can read the server request content.
		HttpServletResponseCopier wrapper = WebUtils.getNativeResponse(response, HttpServletResponseCopier.class);
		if (wrapper != null) {
			byte[] copy = wrapper.getCopy();
			if (copy.length > 0) {
				try {
					return new String(copy, response.getCharacterEncoding());
				} catch (UnsupportedEncodingException e) {
					// NOP
				}
			}
		}
		return "[unknown]";
	}

}
