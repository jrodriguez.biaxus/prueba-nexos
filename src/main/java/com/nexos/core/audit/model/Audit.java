package com.nexos.core.audit.model;

import java.util.Date;

import javax.persistence.Column;

import com.nexos.core.model.CoreEntity;

/**
 * Audit entity to save all the request and responses.
 */
@javax.persistence.Entity
public class Audit extends CoreEntity {

	/** The session id. */
	private String sessionId;

	/** The ip. */
	private String ip;

	/** The request attributes. */
	@Column(columnDefinition = "varchar(MAX)")
	private String attributes;

	/** The request parameters. */
	@Column(columnDefinition = "varchar(MAX)")
	private String parameters;

	/** The request headers. */
	@Column(columnDefinition = "varchar(MAX)")
	private String requestHeaders;

	/** The response headers. */
	@Column(columnDefinition = "varchar(MAX)")
	private String responseHeaders;

	/** The response body. */
	@Column(columnDefinition = "varchar(MAX)")
	private String responseBody;

	/** The response status. */
	private String responseStatus;

	/** The HTTP method. GET, POST, PUT */
	private String method;

	/** The requesting url. */
	private String url;

	/** The timestamp when the request occurs. */
	private Date timestamp;

	/**
	 * Gets the session id.
	 *
	 * @return the session id
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Sets the session id.
	 *
	 * @param sessionId the new session id
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * Gets the ip.
	 *
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Sets the ip.
	 *
	 * @param ip the new ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * Gets the attributes.
	 *
	 * @return the attributes
	 */
	public String getAttributes() {
		return attributes;
	}

	/**
	 * Sets the attributes.
	 *
	 * @param attributes the new attributes
	 */
	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	/**
	 * Gets the parameters.
	 *
	 * @return the parameters
	 */
	public String getParameters() {
		return parameters;
	}

	/**
	 * Sets the parameters.
	 *
	 * @param parameters the new parameters
	 */
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	/**
	 * Gets the request headers.
	 *
	 * @return the request headers
	 */
	public String getRequestHeaders() {
		return requestHeaders;
	}

	/**
	 * Sets the request headers.
	 *
	 * @param headers the new request headers
	 */
	public void setRequestHeaders(String headers) {
		this.requestHeaders = headers;
	}

	/**
	 * Gets the response headers.
	 *
	 * @return the response headers
	 */
	public String getResponseHeaders() {
		return responseHeaders;
	}

	/**
	 * Sets the response headers.
	 *
	 * @param headers the new response headers
	 */
	public void setResponseHeaders(String headers) {
		this.responseHeaders = headers;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the timestamp.
	 *
	 * @param timestamp the new timestamp
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Gets the method.
	 *
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Sets the method.
	 *
	 * @param method the new method
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Gets the response body.
	 *
	 * @return the response body
	 */
	public String getResponseBody() {
		return responseBody;
	}

	/**
	 * Sets the response body.
	 *
	 * @param responseBody the new response body
	 */
	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	/**
	 * Gets the response status.
	 *
	 * @return the response status
	 */
	public String getResponseStatus() {
		return responseStatus;
	}

	/**
	 * Sets the response status.
	 *
	 * @param responseStatus the new response status
	 */
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	/**
	 * String representation for audit entity
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Audit [sessionId=" + sessionId + ", ip=" + ip + ", attributes=" + attributes + ", parameters="
				+ parameters + ", headers=" + requestHeaders + ", url=" + url + ", timestamp=" + timestamp + ", id="
				+ id + ", name=" + name + "]";
	}

}
