package com.nexos.core.audit.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * A web filter to replace the servlet request, for the wrapper copier servlet
 * request
 */
@Component
@Order(1)
public class AuditWebRequestFilter implements javax.servlet.Filter {

	/**
	 * Execute the replace to the original servlet request
	 *
	 * @param request  the servlet request
	 * @param response the servlet response to be replaced
	 * @param chain    the filter chain 
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletResponseCopier responseCopier = new HttpServletResponseCopier((HttpServletResponse) response);
		chain.doFilter(request, responseCopier);
		responseCopier.flushBuffer();
	}

}
