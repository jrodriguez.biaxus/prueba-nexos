package com.nexos.core.audit.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;

/**
 * This is a helper implementation of Servlet Output Stream to do a copy of the
 * native Servlet Output Stream content.
 */
public class ServletOutputStreamCopier extends ServletOutputStream {

	/** The output stream. */
	private OutputStream outputStream;

	/** The copy. */
	private ByteArrayOutputStream copy;

	/**
	 * Instantiates a new servlet output stream copier.
	 *
	 * @param outputStream the output stream
	 */
	public ServletOutputStreamCopier(OutputStream outputStream) {
		this.outputStream = outputStream;
		this.copy = new ByteArrayOutputStream(2048);
	}

	/**
	 * Inherit method to white a single bit. Always maintaining the original and the copy updated
	 *
	 * @param b the bytes to be write
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void write(int b) throws IOException {
		outputStream.write(b);
		copy.write(b);
	}

	/**
	 * Gets the copy.
	 *
	 * @return the copy
	 */
	public byte[] getCopy() {
		return copy.toByteArray();
	}

	/**
	 * Checks if is ready.
	 *
	 * @return true, if is ready
	 */
	@Override
	public boolean isReady() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Sets the write listener.
	 *
	 * @param listener the new write listener
	 */
	@Override
	public void setWriteListener(WriteListener listener) {
		// TODO Auto-generated method stub

	}

}