package com.nexos.core.audit.filter;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * HttpServletResponseWrapper implementation to wrap the original
 * ServletResponse and have a intern copy
 */
public class HttpServletResponseCopier extends HttpServletResponseWrapper {

	/** The original output stream. */
	private ServletOutputStream outputStream;

	/** The writer. */
	private PrintWriter writer;

	/** The copier ouput stream. */
	private ServletOutputStreamCopier copier;

	/**
	 * Instantiates a new http servlet response copier.
	 *
	 * @param response the response
	 */
	public HttpServletResponseCopier(HttpServletResponse response) throws IOException {
		super(response);
	}

	/**
	 * Gets the output stream.
	 *
	 * @return the output stream
	 */
	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		if (writer != null) {
			throw new IllegalStateException("getWriter() has already been called on this response.");
		}

		if (outputStream == null) {
			outputStream = getResponse().getOutputStream();
			copier = new ServletOutputStreamCopier(outputStream);
		}

		return copier;
	}

	/**
	 * Gets the writer.
	 *
	 * @return the writer
	 */
	@Override
	public PrintWriter getWriter() throws IOException {
		if (outputStream != null) {
			throw new IllegalStateException("getOutputStream() has already been called on this response.");
		}

		if (writer == null) {
			copier = new ServletOutputStreamCopier(getResponse().getOutputStream());
			writer = new PrintWriter(new OutputStreamWriter(copier, getResponse().getCharacterEncoding()), true);
		}

		return writer;
	}

	/**
	 * Flush buffer.
	 */
	@Override
	public void flushBuffer() throws IOException {
		if (writer != null) {
			writer.flush();
		} else if (outputStream != null) {
			copier.flush();
		}
	}

	/**
	 * Gets the copy, with all the bytes of servlet output stream.
	 *
	 * @return the copy of the bytes.
	 */
	public byte[] getCopy() {
		if (copier != null) {
			return copier.getCopy();
		} else {
			return new byte[0];
		}
	}

}
