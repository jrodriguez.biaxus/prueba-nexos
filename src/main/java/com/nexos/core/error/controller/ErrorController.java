package com.nexos.core.error.controller;

import java.io.FileNotFoundException;
import java.sql.SQLException;

import javax.activity.InvalidActivityException;
import javax.persistence.PersistenceException;
import javax.sql.rowset.spi.SyncFactoryException;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.boot.json.JsonParseException;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Rest controller to expose the services, this contain a functionality to
 * generate random fails, this is for testing the error loggin.
 */
@org.springframework.web.bind.annotation.RestController()
public class ErrorController {

	/**
	 * Throws a generic error to be tested on the exception hanldler
	 *
	 * @throws FileNotFoundException    the file not found exception
	 * @throws InvalidActivityException the invalid activity exception
	 * @throws SQLException             the SQL exception
	 */
	@GetMapping("/randomError")
	public void findAllFamily() throws FileNotFoundException, InvalidActivityException, SQLException {
		double rand = Math.random();
		if (rand < 0.1)
			throw new NullPointerException();
		if (rand < 0.2)
			throw new FileNotFoundException();
		if (rand < 0.3)
			throw new PersistenceException();
		if (rand < 0.4)
			throw new InvalidActivityException();
		if (rand < 0.5)
			throw new JsonParseException();
		if (rand < 0.6)
			throw new HibernateException("Nothing");
		if (rand < 0.7)
			throw new SQLException();
		if (rand < 0.8)
			throw new SyncFactoryException();
		if (rand < 0.9)
			throw new BeanCreationException("Nothing");
	}
}
