
package com.nexos.core.error.model;

import javax.persistence.Column;

import com.nexos.core.model.CoreEntity;

/**
 * Entity to save the errors on db
 */
@javax.persistence.Entity
public class Error extends CoreEntity {

	/** The current session id when the error ocurrs. */
	private String sessionId;

	/** The ip of client. */
	private String ip;

	/** The detailed message of exception . */
	private String message;

	/** The date time value on the exception occurs. */
	private java.util.Date timestamp;

	/** The full stack trace of exception. */
	@Column(columnDefinition = "varchar(MAX)")
	private String stackTrace;

	/**
	 * Gets the session id.
	 *
	 * @return the session id
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Sets the session id.
	 *
	 * @param sessionId the new session id
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * Gets the ip.
	 *
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Sets the ip.
	 *
	 * @param ip the new ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the stack trace.
	 *
	 * @return the stack trace
	 */
	public String getStackTrace() {
		return stackTrace;
	}

	/**
	 * Sets the stack trace.
	 *
	 * @param stackTrace the new stack trace
	 */
	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

	/**
	 * Gets the date time.
	 *
	 * @return the date time
	 */
	public java.util.Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the date time.
	 *
	 * @param timestamp the new date time
	 */
	public void setTimestamp(java.util.Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * String representation of the entity error
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Error [sessionId=" + sessionId + ", ip=" + ip + ", message=" + message + ", stackTrace=" + stackTrace
				+ ", id=" + id + ", name=" + name + "]";
	}

}
