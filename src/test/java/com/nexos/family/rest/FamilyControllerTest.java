package com.nexos.family.rest;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexos.Application;
import com.nexos.family.model.Family;
import com.nexos.family.model.Person;

/**
 * A test class to test the family controller
 * 
 * @author jr 20200304
 */
@SpringBootTest(classes = { Application.class })
public class FamilyControllerTest {

	/**
	 * Field to contains the rest client to test the exposed endpoints
	 * 
	 * @author jr 20200304
	 */
	MockMvc mockMvc;

	/**
	 * Method to bootstrap the mvc client
	 * 
	 * @author jr 20200304
	 */
	@BeforeEach
	void setup(WebApplicationContext wac) {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	/**
	 * Tries to get the family list
	 */
	@Test
	void tryFindFamlilyList() throws Exception {
		// @formatter:off
		this.mockMvc.perform(get("/families")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"));
		// @formatter:on
	}

	/**
	 * Tries to save a person. Have sceneries like, adding with no name, and any
	 * required fields. Also person creation, and person creation with family
	 * association
	 */
	@Test
	void trySavePerson() throws Exception {

		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		// @formatter:off
		this.mockMvc.perform(post("/person")
				.params(params)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").isArray())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("documentNumber can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("mobilePhone can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("phone can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("name can't be empty")));
		// @formatter:on

		params = new LinkedMultiValueMap<>();
		params.add("name",
				"Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson RodriguezJeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson RodriguezJeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez Jeferson Rodriguez");
		// @formatter:off
		this.mockMvc.perform(post("/person")
				.params(params)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").isArray())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("documentNumber can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("mobilePhone can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("phone can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("name only can be 400 character length")));
		// @formatter:on

		params = new LinkedMultiValueMap<>();
		params.add("name", "Jeferson Rodriguez");
		// @formatter:off
		this.mockMvc.perform(post("/person")
				.params(params)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").isArray())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("documentNumber can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("mobilePhone can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("phone can't be empty")));
		// @formatter:on

		params = new LinkedMultiValueMap<>();
		params.add("name", "Jeferson Rodriguez");
		params.add("mobilePhone", "102156321546121661632165");
		// @formatter:off
		this.mockMvc.perform(post("/person")
				.params(params)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").isArray())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("documentNumber can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("mobilePhone only can be 10 character length")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("phone can't be empty")));
		
		params = new LinkedMultiValueMap<>();
		params.add("name", "Jeferson Rodriguez");
		params.add("mobilePhone", "3043607400");
		// @formatter:off
		this.mockMvc.perform(post("/person")
				.params(params)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").isArray())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("documentNumber can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("phone can't be empty")));
		// @formatter:on

		params = new LinkedMultiValueMap<>();
		params.add("name", "Jeferson Rodriguez");
		params.add("mobilePhone", "3043607400");
		params.add("phone", "3043607400");
		// @formatter:off
		this.mockMvc.perform(post("/person")
				.params(params)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").isArray())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("documentNumber can't be empty")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("phone only can be 7 character length")));
		// @formatter:on

		params = new LinkedMultiValueMap<>();
		params.add("name", "Jeferson Rodriguez");
		params.add("mobilePhone", "3043607400");
		params.add("phone", "4301738");
		// @formatter:off
		this.mockMvc.perform(post("/person")
				.params(params)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").isArray())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("documentNumber can't be empty")));
		// @formatter:on

		params = new LinkedMultiValueMap<>();
		params.add("name", "Jeferson Rodriguez");
		params.add("mobilePhone", "3043607400");
		params.add("phone", "4301738");
		params.add("documentNumber", "1321321321312321321");
		// @formatter:off
		this.mockMvc.perform(post("/person")
				.params(params)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").isArray())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors",hasItem("documentNumber only can be 12 character length")));
		// @formatter:on

		params = new LinkedMultiValueMap<>();
		params.add("name", "Jeferson Rodriguez");
		params.add("documentNumber", "1051562226");
		params.add("mobilePhone", "3043607400");
		params.add("phone", "4301738");
		// @formatter:off
		MvcResult result = this.mockMvc.perform(post("/person")
				.params(params)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andReturn();
		// @formatter:on
		MockHttpServletResponse r = result.getResponse();
		ObjectMapper om = new ObjectMapper();
		Person person = om.readValue(r.getContentAsString(), Person.class);
		assertNotNull(person.getId());
		// @formatter:off
		result = this.mockMvc.perform(get("/families")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andReturn();
		// @formatter:on

		r = result.getResponse();
		List<Family> family = om.readValue(r.getContentAsString(), new TypeReference<List<Family>>() {
		});

		params = new LinkedMultiValueMap<>();
		params.add("name", "Xilena Rodriguez");
		params.add("documentNumber", "1051512226");
		params.add("mobilePhone", "3043607400");
		params.add("phone", "4301738");
		params.add("family.id", family.get(0).getId().toString());
		// @formatter:off
		result = this.mockMvc.perform(post("/person")
				.params(params)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType("application/json"))
				.andReturn();
		// @formatter:on
		r = result.getResponse();
		Person person2 = om.readValue(r.getContentAsString(), Person.class);

		assertNotNull(person2.getId());
	}

}
