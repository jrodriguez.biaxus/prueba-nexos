package com.nexos.family.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import javax.servlet.ServletException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.nexos.Application;
import com.nexos.family.model.Family;
import com.nexos.family.model.Person;

/**
 * A test class to test the family service
 * 
 * @author jr 20200304
 */
@SpringBootTest(classes = {Application.class})
public class FamilyServiceTest {

	/**
	 * Field to contains the service to be tested
	 * 
	 * @author jr 20200304
	 */
	@Autowired
	private FamilyService service;

	/**
	 * Verify if the context dependency injection works.
	 */
	@Test
	void contextLoads() {
		assertThat(service).isNotNull();
	}

	/**
	 * Try save persons. Have some test cases, person with no family and person with
	 * family
	 * @throws ServletException Validation errors
	 */
	@Test
	@Rollback
	void trySaveFamilyAndPerson() throws ServletException {

		Person person = new Person();
		person.setName("Jeferson Rodriguez");
		person.setDocumentNumber("1051823226");
		person.setMobilePhone("3043607400");
		person.setPhone("4301738");

		service.savePerson(person);
		assertNotNull(person.getId());
		assertNotNull(person.getFamily());
		assertNotNull(person.getFamily().getId());

		Family family = person.getFamily();

		person = new Person();
		person.setName("Xilena Rodriguez");
		person.setDocumentNumber("1051823226");
		person.setMobilePhone("3043607400");
		person.setPhone("4301738");
		person.setFamily(family);
		service.savePerson(person);
		assertNotNull(person);
		assertNotNull(person.getFamily());
		assertNotNull(person.getFamily().getId());

	}

	/**
	 * Try query the family list
	 */
	@Test
	void tryFindAllFamily() {
		List<Family> familyList = service.findAllFamily();
		assertNotNull(familyList);
	}

}
