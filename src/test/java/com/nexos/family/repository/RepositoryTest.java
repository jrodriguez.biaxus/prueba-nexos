package com.nexos.family.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.nexos.family.model.Family;
import com.nexos.family.model.Person;

/**
 * Test class for repository class
 */
@DataJpaTest
class RepositoryTest {

	/** Core repository to be tested. */
	@Autowired
	CoreEntityRepository repository;

	/** Family repository to be tested. */
	@Autowired
	FamilyRepository familyRepository;

	/**
	 * Verify if the context dependency injection works.
	 */
	@Test
	void contextLoads() {
		assertThat(repository).isNotNull();
	}

	/**
	 * Try save a family and a person. Have some test cases, saving family saving
	 * person with no family and assigning person to family
	 */
	@Test
	void trySaveFamilyAndPerson() {
		Family family = new Family();
		family.setName("Familia Rodriguez");
		repository.saveAndFlush(family);
		assertNotNull(family.getId());

		Person person = new Person();
		person.setName("Jeferson Rodriguez");
		person.setDocumentNumber("1051823226");
		person.setMobilePhone("3043607400");
		person.setPhone("4301738");

		repository.saveAndFlush(person);
		assertNotNull(person);

		person.setFamily(family);
		repository.saveAndFlush(person);

		assertNotNull(person.getFamily().getId());

	}

	/**
	 * Try query the family list
	 */
	@Test
	void tryFindAllFamily() {
		List<Family> familyList = familyRepository.findAll();
		assertNotNull(familyList);
	}

}
